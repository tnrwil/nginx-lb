data "terraform_remote_state" "nginx" {
  backend = "s3"
  config = {
    bucket = "cie-test-tf-nginx"
    key    = "backend/terraform.tfstate"
    region = "us-gov-west-1"
    }
  }