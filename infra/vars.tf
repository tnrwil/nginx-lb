variable "playbook_path"{
  default = "sanbox/infra/rhel_vm_provisioning.yaml"
}

variable "aws_region"{
  description = "this is the default region we are using"
  default = "us-gov-west-1"
}

variable "aws_ami"{
  default="ami-5a740e3b"
  #default="ami-1c81ce7d"
}

variable "instance_count" {
  default="1"
}
variable "ec2_type" {
  default="t2.small"
}
variable "ec2_key_name" {
  default="tam-2-govcloud"
}

variable "pub_key" {
  default="tam-2-govcloud.pem"
}

variable "pub_key_bucket_name" {
  default="cie-mis"
}

variable "ssh_user" {
  default="ec2-user"
}

variable "ebs_vol_size" {
  default="100"
}

variable "private_subnet" {
  #TestVPC
  default="subnet-9d3d7deb"
  #PRODVPC
  #default="subnet-ce688187"
}

variable "public_subnet" {
  #TestVPC
  default="subnet-3b37774d"
  #PRODVPC
  #default="subnet-c606ef8f"
}

variable "vpc" {
  default = "vpc-8ad750ee"
}


variable "vpc_security_group_ids" {
  type    = list(string)
  default = ["sg-b9f641c0"]
}

#variable "security_groups" {
#  type = list (object({
#    sec_group1 = string
#    sec_group2 = string
#    sec_group3 = string
#  }))
#  default = [
#    {
#      sec_group1 = "sg-12068674"
#      sec_group2 = "sg-17e1216e"
#      sec_group3 = "sg-d102c2a8" 
#    }
#  ]
#}
