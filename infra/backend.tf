terraform {
backend "s3" {
  bucket = "cie-test-tf-nginx"
  key    = "backend/terraform.tfstate"
  region = "us-gov-west-1"
  }
}