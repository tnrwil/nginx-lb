# Log into AWS
provider "aws" {
#  required_version = "~> 2.0"
  region = "${var.aws_region}"
}

data "aws_s3_bucket_object" "pub-key" {
  bucket = "${var.pub_key_bucket_name}"
  key = "${var.pub_key}"
}

resource "aws_instance" "NGINX-LB" {
  #count = "${var.instance_count}"
  ami           = "${var.aws_ami}"
  instance_type = "${var.ec2_type}"
  subnet_id = "${var.public_subnet}"
  vpc_security_group_ids = "${var.vpc_security_group_ids}"
  key_name = "${var.ec2_key_name}"
  root_block_device {
    volume_size = "${var.ebs_vol_size}"
  }
  timeouts {
    create = "60m"
    update = "60m"
  }
  provisioner "remote-exec" {
    inline = [
      "echo '${self.public_ip}' >> hosts.txt",
      ]
      connection {
      type = "ssh"
      user = "${var.ssh_user}"
      private_key = "${file(var.pub_key)}"
      host = "${self.public_ip}"
      timeout = "10m"
      }
    }
   tags = {
     Name = "NGINX-LB"
     1067 = "N/A"
     Application = "NGINX"
     Application_Role = "Service Management"
     Cost_Center = "AFLCMC, AFRL"
     Email = "dpage@expansiagroup.com"
     Environment = "TEST"
     Group = "CIE, HBG, RIEBB"
     OS = "RHEL_7"
     Organization = "AF DCGS, AFRL"
     Owner = "Dave Page"
     Project = "CI/CD"
   }
}

output "ec2_public_ip" {
 value = aws_instance.NGINX-LB.public_ip
}

output "ec2_private_ip" {
 value = aws_instance.NGINX-LB.private_ip
}

 output "ec2_public_dns" {
 value = aws_instance.NGINX-LB.public_dns
}
