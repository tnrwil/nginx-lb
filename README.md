# NGINX Deployment Project

## This project is configured with an nginx-lb role and leverages templates.
## This project will provision and configure an EC2 via Terraform then install and configure NGINX via Ansible.  
**Additional work will be required to reuse this project on-prem as it currently relies on internet access**

Varibles are defined under infra for Terraform and nginx-lb/vars/main.yml for the playbook.

Templates are in nginx-lb/templates and leverage Jinja to set variables.  
*The variables are dynamically assigned **(ie - not defined in the vars/main.yml)** via set_fact in the playbook for the template*  
*Templates in this playbook == **/etc/nginx/niginx.config** and is passed in via the template play*  